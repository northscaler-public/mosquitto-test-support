#!/usr/bin/env sh

THIS_DIR="$(cd "$(dirname "$0")"; pwd)"

if { [ -n "$GITHUB_SHA" ] || [ -n "$CI_COMMIT_SHA" ]; }; then # we're in CI pipeline
  echo 'in pipeline - mosquitto is started'
else
  CONTAINER=${1:-$MOSQUITTO_TEST_SUPPORT_MOSQUITTO_HOST}
  if [ -z "$CONTAINER" ]; then
    CONTAINER="$(cat $THIS_DIR/default-mosquitto-test-container)"
  fi
  PORT=${2:-$MOSQUITTO_TEST_SUPPORT_MOSQUITTO_PORT}
  if [ -z "$PORT" ]; then
    PORT="$(cat $THIS_DIR/default-mosquitto-test-port)"
  fi
  if [ -z "$(docker ps --quiet --filter name=$CONTAINER)" ]; then
    "$THIS_DIR/start-mosquitto-container.sh" "$CONTAINER" $PORT
  fi
fi
