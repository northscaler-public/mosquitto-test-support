'use strict'

const mosquittoConnect = require('./mosquitto-connect')
module.exports = {
  mosquittoConnect,
  defaultPort: mosquittoConnect.defaultPort,
  defaultContainerName: mosquittoConnect.defaultContainerName
}
