'use strict'

const fs = require('fs')
const _ = require('lodash')
const mqtt = () => require('mqtt')
const startMosquitto = require('./start-mosquitto')

let connection

const fn = async ({
  protocol = 'mqtt://',
  host = 'localhost',
  port = fn.defaultPort,
  containerName = fn.defaultContainerName,
  opts = {
  }
} = {}) => {
  if (connection) return connection

  const scriptArgs = [containerName, port]
  await startMosquitto({ scriptArgs })

  const url = `${protocol}${host}:${port}`

  opts = _.cloneDeep(opts) || {}
  if (!opts.auth?.user || !opts.auth?.password) {
    try { delete opts.auth } catch (e) { /* gulp */ }
  }

  const mosquittoClient = await mqtt().connect(url, opts)

  connection = mosquittoClient

  return connection
}

fn.defaultPort = parseInt(fs.readFileSync(`${__dirname}/default-mosquitto-test-port`))
fn.defaultContainerName = fs.readFileSync(`${__dirname}/default-mosquitto-test-container`)

module.exports = fn
