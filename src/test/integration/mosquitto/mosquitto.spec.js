/* global describe, it */
'use strict'

const chai = require('chai')
chai.use(require('dirty-chai'))
const expect = chai.expect

const m = require('../../../main')

describe('integration tests of mosquitto', function () {
  describe('mosquitto-connect', function () {
    it('should work', async function () {
      if (process.env.CI) {
        console.log('not running tests because in CI pipeline')
        return
      }

      this.timeout(5000)

      const client = await m.mosquittoConnect({
        host: 'localhost',
        port: process.env.MOSQUITTO_TEST_SUPPORT_MOSQUITTO_PORT || m.mosquittoConnect.defaultPort
      })

      expect(client).to.be.ok()
    })
  })

})
