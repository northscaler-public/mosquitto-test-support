# `mosquitto-test-support`

Handy-dandy Mosquitto integration testing utility that starts a local Docker container running mosquitto if you're not running in a CI/CD pipeline.
This allows you to run integration tests locally in a manner similar to how they'd be run in the CI/CD pipeline. 
This module does nothing when running in a CI build pipeline, because Mosquitto should be configured as part of the build via something like [`.gitlab-ci.yml`'s `services`](https://docs.gitlab.com/ee/ci/yaml/#services) element.

This package is intended to be installed in your project in `devDependencies`.

Your application must install its desired versions of [`mqtt`](https://www.npmjs.com/package/mqtt).

> NOTE: requires a Unix-y shell (`/usr/bin/env sh`) to be available.
>This is not designed to run on Windows; PRs/MRs welcome.

Usage:
```javascript
const { mosquittoConnect } = require('@northscaler/mosquitto-test-support')

const client = await mosquittoConnect()
```

Usage to work in both local environment & CI pipeline:
```javascript
const { mosquittoConnect } =  require('@northscaler/mosquitto-test-support')
const client = await mosquittoConnect(process.env.CI_COMMIT_SHA ? { host: 'mosquitto', port: 1883 } : undefined)
```

## Configuration

The default configuration is pretty conventional, with the sole exception of the default port that mosquitto will listen on for clients.
Instead of `1883`, which might already be in use on developers' machines when they run integration tests, the default configuration uses `2883`.
It is a `TODO` to search for an available port.

>NOTE: This module detects when it's running in a CI/CD pipeline by seeing if the environment variable `CI_COMMIT_SHA` or `GITHUB_SHA` is of nonzero length.

### Environment variables

The following environment variables can be set to configure the docker container:
* MOSQUITTO_TEST_SUPPORT_TAG: The tag of the [`eclipse-mosquitto` Docker image](https://hub.docker.com/_/eclipse-mosquitto)  or custom image to use, default `latest`
* MOSQUITTO_TEST_SUPPORT_PORT: visible client port on `localhost` to map to container port, default is content of `mosquitto/default-mosquitto-test-port`
* MOSQUITTO_TEST_SUPPORT_CONTAINER: name of container, default is content of file `mosquitto/default-mosquitto-test-container`
* MOSQUITTO_TEST_SUPPORT_CONTAINER_PORT: mosquitto client port in container, default `9042`
* MOSQUITTO_TEST_SUPPORT_IMAGE: docker image name, default `mosquitto`
